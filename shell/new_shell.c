#define _GNU_SOURCE
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int *new_shell(char **env)
{
  int pipe_rw[2];
  int pipe_wr[2];

  int *fd = (int *)malloc(3 * sizeof(int));
  pipe(pipe_rw);
  pipe(pipe_wr);
  if(!(fd[2] = fork()))
    {
      close(pipe_wr[1]);
      dup2(pipe_wr[0], 0);

      close(pipe_rw[0]);      
      dup2(pipe_rw[1], 1);
      //      splice(0, 0, 1, NULL, 3, 0);
      execve("/bin/sh", NULL, NULL);
    }
  close(pipe_rw[1]);
  fd[0] = pipe_rw[0];

  close(pipe_wr[0]);
  fd[1] = pipe_wr[1];

  return fd;
}
/*
int main(int argc, char **argv, char **env)
{
  char msg[1024];
  int *fd;
  
  fd = new_shell(env);
  write(fd[1], "echo COCO\n", strlen("echo COCO\n"));
  read(fd[0], msg, 1024); 
  printf("%s\n", msg);

  write(fd[1], "ls -l\n", strlen("ls -l\n"));
  read(fd[0], msg, 1024);
  printf("%s\n", msg);
}
*/
