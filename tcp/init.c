int tcp_init_clt(char *ip)
{
  int sock;
  struct sockaddr_in sin;

  printf("ip = :%s\n:", ip);
  if ((sock = socket(PF_INET, SOCK_STREAM, 0)) == -1)
    {
      perror("socket :");
      return (0);
    }

  sin = (struct sockaddr_in) {.sin_addr.s_addr = inet_addr(ip),
			      .sin_family = PF_INET,
			      .sin_port = htons(TCP_PORT)};

  if (connect(sock, (struct sockaddr*)&sin, sizeof(struct sockaddr)) == -1)
    {
      perror("connect :");
      close(sock);
      return (0);
    }
  return (sock);
}

int tcp_init_srv()
{
  	int sock;
	struct sockaddr_in serveur;
	struct sockaddr_in client;
	unsigned int size;

	if ((sock = socket(PF_INET, SOCK_STREAM, 0)) == -1)
	{
		printf("socket\n");
		return (NULL);
	}
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int));
	serveur = (struct sockaddr_in) {.sin_family = PF_INET,
				   .sin_port = htons(TCP_PORT)};
	size = sizeof(struct sockaddr);
	if (bind(sock, (struct sockaddr *)&serveur, size) == -1)
	  perror("bind :");
	else if (listen(sock, 0) == -1)
	  perror("listen :");
	else if ((sock = accept(sock, (struct sockaddr *)&client, &size)) == -1)
	  perror("accept :");
	else
	  return (sock);
	close(sock);
	return (0);
}
