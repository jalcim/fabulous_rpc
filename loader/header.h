#ifndef __H_HEAD__
#define __H_HEAD__

typedef struct s_loader t_loader;
typedef struct s_object t_object;
typedef struct s_infolib t_infolib;
typedef struct s_module t_module;

struct s_loader
{
  void *handle;
  m_init_f init;
  char **tab_method;
};

struct s_object
{
  m_method_f syscall[256];
  void *data[256];
};

struct s_infolib
{
  char name[256];
  char init[256];
};

struct s_module
{
  t_loader loader;
  t_object object;
  t_infolib infolib;
};

int init(t_loader *loader, t_object *object, t_infolib *infolib);
#endif
