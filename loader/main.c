#include <test.h>
#include <dlfcn.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "header.h"

void exec(char *tab, t_loader *loader, t_object *object)
{
  int i ;

  i = -1;
  while(tab[++i])
    object->syscall[(int)tab[i]](NULL);
  object->syscall[(int)tab[i]](NULL);
}

void destruct(t_loader *loader, t_object *object)
{
  dlclose(loader->handle);
  free(loader->tab_method);
}

int main(int argc, char **argv)
{
  t_module *module = (t_module *)malloc(sizeof(t_module));
  char tab[8] = {2,1,2,1,2,1,0,2};//demo

  module->infolib = (t_infolib) {.name = "libtest.so", .init = "m_init"};
  init(&module->loader, &module->object, &module->infolib);
  exec(tab, &module->loader, &module->object);//demo
  destruct(&module->loader, &module->object);
  free(module);
  return (0);
  }

int init(t_loader *loader, t_object *object, t_infolib *infolib)
{
  int i = -1;
  
  *loader = (t_loader) {0};
  *object = (t_object) {0};

  if (!(loader->handle = dlopen(infolib->name, RTLD_LAZY)))
    {
      printf("An error occured: %s\n", dlerror ());
      free(loader);
      free(object);
      return (-1);
    }

  if (!(loader->init = dlsym(loader->handle, infolib->init)))
    {
      printf("An error occured: %s\n", dlerror ());
      dlclose(loader->handle);
      destruct(loader, object);
      return (-1);
    }
  loader->tab_method = loader->init();
  while (loader->tab_method[++i])
    {
      if (!(object->syscall[i] = dlsym(loader->handle, loader->tab_method[i])))
	{
	  printf("An error occured: %s\n", dlerror ());
	  dlclose(loader->handle);
	  destruct(loader, object);
	  return (-1);
	}
    }
  object->syscall[i] = NULL;
  return (0);
}
