compil_lib()
{
    cd lib
    gcc -fPIC -c -Wall -I. test.c -g
    gcc -shared -Wl,-soname,libtest.so.1 -Wall test.o -o ../build/lib/libtest.so.1.0 -g
    rm test.o
    ln -s ../build/lib/libtest.so.1.0 ../build/lib/libtest.so
    ln -s ../build/lib/libtest.so.1.0 /usr/lib/libtest.so.1.0
    ln -s ../build/lib/libtest.so.1.0 /usr/lib/libtest.so
    cp test.h /tmp/fstest/include
    cd ..
}

compil_loader()
{
    cd loader
    gcc -I/tmp/fstest/include -Wall main.c -ldl  -o ../test_dlopen -g
    cd ..
}

rm -rf /tmp/fstest
rm -rf build
mkdir -p /tmp/fstest/include
mkdir -p build/loader build/lib

compil_lib
compil_loader
